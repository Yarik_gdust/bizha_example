Meteor.publish('posts', function() {
  // check(options, {
  //   sort: Object,
  //   limit: Number
  // });
  return Posts.find({private: 0});
});

Meteor.publish('singlePost', function(id) {
  check(id, String);
  return Posts.find(id);
});

Meteor.publish('privatePosts', function() {
   // check(private, Number);
  return [
  Posts.find({private: 1}),
  
  ];
});

Meteor.publish('wishPosts', function() {
   // check(wishlist, Number);
  return Posts.find({ wishlist: 1});
});


Meteor.publish('comments', function(postId) {
  check(postId, String);
  return [
  Comments.find({postId: postId}),
  Posts.find({private: 0})
  ];
});

Meteor.publish('notifications', function() {
  return Notifications.find({userId: this.userId, read: false});
});
