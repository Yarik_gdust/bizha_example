Template.postSubmit.onCreated(function() {
  Session.set('postSubmitErrors', {});
});

Template.postSubmit.helpers({
  errorMessage: function(field) {
    return Session.get('postSubmitErrors')[field];
  },
  errorClass: function (field) {
    return !!Session.get('postSubmitErrors')[field] ? 'has-error' : '';
  },
  findPosts() {
  var stuff = Posts.find({});
  return stuff
    }
});

Template.postSubmit.events({

  'submit form': function(e) {
    e.preventDefault();
    var value = ('true' === $(".chk").val() );
    var post = {
      image: $(e.target).find('[name=image]').val(),
      image2: $(e.target).find('[name=image2]').val(),
      image3: $(e.target).find('[name=image3]').val(),
      description: $(e.target).find('[name=description]').val(),
      title: $(e.target).find('[name=title]').val(),
      private: Number($("input[name='private']:checked").val()) ? 1 : 0,
      price: $(e.target).find('[name=price]').val() ? $(e.target).find('[name=price]').val() : 'уточняйте цену',
      wishlist: 0

    };
    
    
    Meteor.call('postInsert', post, function(error, result) {

      Router.go('postPage', {_id: result._id});  
    });
  }

});